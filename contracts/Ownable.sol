pragma solidity ^0.8.13;

contract OwnableCustom {
    address payable owner;

    constructor() {
        owner = payable(msg.sender);
    }

    modifier onlyOwner() {
        require(isOwner(), "You are not the owner");
        _;
    }

    function isOwner() public view returns (bool) {
        return (msg.sender == owner);
    }
}
