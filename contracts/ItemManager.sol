pragma solidity ^0.8.13;
pragma experimental ABIEncoderV2;

import "./Ownable.sol";
import "./Item.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract ItemManager is OwnableCustom, ReentrancyGuard {
    struct S_Item {
        Item _item;
        string _identifier;
        uint256 _itemPrice;
        ItemManager.SupplyChainSteps _step;
        address _customerAddress;
    }

    mapping(address => S_Item[]) public items;

    enum SupplyChainSteps {
        CREATED,
        PAID,
        DELIVERED
    }

    event SupplyChainStep(
        address _itemAddress,
        address _customerAddress,
        uint256 _step
    );

    function createItem(string memory _identifier, uint256 _itemPrice)
        public
        nonReentrant
        onlyOwner
    {
        pushItemToAddress(msg.sender, _itemPrice, _identifier);
    }

    // TODO lspiller is this bad style in soldity? should one rather use an event for this kind of scenario?
    function getItemsForCustomerAddress(address _customerAddress)
        public
        view
        returns (S_Item[] memory)
    {
        return S_Item[](items[_customerAddress]);
    }

    function getItemByAddress(address _itemAddress)
        public
        view
        returns (S_Item memory)
    {
        S_Item[] storage _userItems = items[msg.sender];

        for (uint256 i = 0; i < _userItems.length; i++) {
            if (address(_userItems[i]._item) == _itemAddress) {
                return _userItems[i];
            }
        }
    }

    /** When assigning a storage reference data to a memory referenced variable,
     we are copying data from storage to memory. New memory is allocated. */
    function triggerPaymentWithAdditionalMemory(address _itemAddress)
        public
        payable
        nonReentrant
    {
        S_Item[] memory _userItems = items[msg.sender];
        Item _item;

        for (uint256 i = 0; i < _userItems.length; i++) {
            if (address(_userItems[i]._item) == _itemAddress) {
                _item = _userItems[i]._item;
                _userItems[i]._step = SupplyChainSteps.PAID;
                items[msg.sender][i] = _userItems[i];

                emit SupplyChainStep(
                    address(_item),
                    msg.sender,
                    uint256(SupplyChainSteps.PAID)
                );
                break;
            }
        }
    }

    /** When assigning a memory referenced data to a storage referenced variable,
     we are copying data from memory to storage. No new storage is created. */
    function triggerPaymentWithAdditionalStorage(address _itemAddress)
        public
        payable
        nonReentrant
    {
        S_Item[] storage _userItems = items[msg.sender];
        Item _item;

        for (uint256 i = 0; i < _userItems.length; i++) {
            if (address(_userItems[i]._item) == _itemAddress) {
                _item = _userItems[i]._item;
                _userItems[i]._step = SupplyChainSteps.PAID;
                //items[msg.sender][i] = _userItems[i];

                emit SupplyChainStep(
                    address(_item),
                    msg.sender,
                    uint256(SupplyChainSteps.PAID)
                );
                break;
            }
        }
    }

    /** Work directly on the storage, without any additional memory or storage */
    function triggerPaymentDirectlyOnStorage(address _itemAddress)
        public
        payable
        nonReentrant
    {
        Item _item;
        //S_Item _ref;
        //address _addr;

        for (uint256 i = 0; i < items[msg.sender].length; i++) {
            if (address(items[msg.sender][i]._item) == _itemAddress) {
                require(
                    items[msg.sender][i]._itemPrice <= msg.value,
                    "Only full payments accepted"
                );
                require(
                    items[msg.sender][i]._step == SupplyChainSteps.CREATED,
                    "Item has already been paid or delivered"
                );
                items[msg.sender][i]._step = SupplyChainSteps.PAID;
                _item = items[msg.sender][i]._item;
                //_addr = address(items[msg.sender][i]._item);

                emit SupplyChainStep(
                    address(_item),
                    msg.sender,
                    uint256(SupplyChainSteps.PAID)
                );
                break;
            }
        }
    }

    function triggerDelivery(address _itemAddress) public {
        S_Item[] storage _userItems = items[msg.sender];

        for (uint256 i = 0; i < _userItems.length; i++) {
            if (address(_userItems[i]._item) == _itemAddress) {
                require(
                    _userItems[i]._step == SupplyChainSteps.PAID,
                    "Item isn't paid yet"
                );
                _userItems[i]._step = SupplyChainSteps.DELIVERED;

                emit SupplyChainStep(
                    address(_userItems[i]._item),
                    msg.sender,
                    uint256(SupplyChainSteps.DELIVERED)
                );
                break;
            }
        }
    }

    function pushItemToAddress(
        address _customerAddress,
        uint256 _itemPrice,
        string memory _identifier
    ) public returns (address) {
        Item _item = new Item(this, _itemPrice, _customerAddress);

        items[_customerAddress].push(
            S_Item(
                _item,
                _identifier,
                _itemPrice,
                SupplyChainSteps.CREATED,
                _customerAddress
            )
        );

        emit SupplyChainStep(
            address(_item),
            _customerAddress,
            uint256(SupplyChainSteps.CREATED)
        );
        return address(_item);
    }
}
