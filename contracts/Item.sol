pragma solidity ^0.8.13;

import "./ItemManager.sol";

contract Item {
    uint256 public priceInWei;
    uint256 public pricePaid;
    address public customerAddress;

    ItemManager parentContract;

    constructor(
        ItemManager _parentContract,
        uint256 _priceInWei,
        address _customerAddress
    ) {
        priceInWei = _priceInWei;
        parentContract = _parentContract;
        customerAddress = _customerAddress;
    }

    // external functions are callable ONLY from the outside, contrary to public which allows external and internal calls
    // the drawback with public functions is that they require more memory and therefor more gas
    receive() external payable {
        require(pricePaid == 0, "Item is paid already");
        require(priceInWei == msg.value, "Only full payments allowed");
        pricePaid += msg.value;
        (bool success, ) = (address(parentContract)).call{value: msg.value}(
            abi.encodeWithSignature("triggerPayment(uint256)")
        );
        require(success == true, "The transaction failed");
    }
}
