const ItemManager = artifacts.require("./ItemManager.sol");
const BN = web3.utils.BN;

contract("ItemManager", accounts => {

    it("... should be able to add and get Items", async () => {
        const itemManagerInstance = await ItemManager.deployed();
        const itemName = "test1";
        const itemName2 = "test2";
        const itemPrice = 500;

        const result = await itemManagerInstance.createItem(itemName, itemPrice, { from: accounts[0] });
        assert.equal(result.logs[0].args._customerAddress, accounts[0], "Wrong address");

        const item = await itemManagerInstance.items(accounts[0], [0]);
        assert.equal(item._identifier, itemName, "Wrong name");

        await itemManagerInstance.createItem(itemName2, itemPrice, { from: accounts[0] });
        const item2 = await itemManagerInstance.items(accounts[0], [1]);
        assert.equal(item2._identifier, itemName2, "wrong name");

        const items = await itemManagerInstance.getItemsForCustomerAddress(accounts[0]);
        assert.equal(items.length, 2, "Wrong number of items in array");
    });

    it("... should be able to pay an Item", async () => {
        const itemPrice = 500;
        const itemManagerInstance = await ItemManager.deployed();

        const result = await itemManagerInstance.createItem("test", itemPrice, { from: accounts[0] });
        let item = await itemManagerInstance.getItemByAddress(result.logs[0].args._itemAddress);
        // console.log("ITEM RETURNED: " + JSON.stringify(item));
        assert.equal(item._item, result.logs[0].args._itemAddress);

        await itemManagerInstance.triggerPaymentDirectlyOnStorage(item._item, { value: web3.utils.toWei(new BN(itemPrice), "wei") });
        item = await itemManagerInstance.getItemByAddress(result.logs[0].args._itemAddress);
        // console.log("ITEM RETURNED: " + JSON.stringify(item));
        assert.equal(item._step, 1, "wrong step");
    });

    it("... should be able to pay an Item with varying gas amount used", async () => {
        const itemPrice = 500;
        const itemManagerInstance = await ItemManager.deployed();

        const result = await itemManagerInstance.createItem("test", itemPrice, { from: accounts[0] });

        const resultPaymentDirectStorage = await itemManagerInstance.triggerPaymentDirectlyOnStorage(result.logs[0].args._itemAddress, { value: web3.utils.toWei(new BN(itemPrice), "wei") });
        const resultPaymentAdditionalMemory = await itemManagerInstance.triggerPaymentWithAdditionalMemory(result.logs[0].args._itemAddress, { value: web3.utils.toWei(new BN(itemPrice), "wei") });
        const resultPaymentAdditionalStorage = await itemManagerInstance.triggerPaymentWithAdditionalStorage(result.logs[0].args._itemAddress, { value: web3.utils.toWei(new BN(itemPrice), "wei") });

        const gasUsedDirectStorage = resultPaymentDirectStorage.receipt.gasUsed;
        const gasUsedAdditionalMemory = resultPaymentAdditionalMemory.receipt.gasUsed;
        const gasUsedAdditionalStorage = resultPaymentAdditionalStorage.receipt.gasUsed;

        console.log("GAS USED WORKING DIRECTLY ON STORAGE: " + gasUsedDirectStorage)
        console.log("GAS USED WORKING WITH ADDITIONAL MEMORY: " + gasUsedAdditionalMemory)
        console.log("GAS USED WORKING WITH ADDITIONAL STORAGE: " + gasUsedAdditionalStorage)
    });

    it("... should be able to trigger delivery", async () => {
        const itemPrice = 500;
        const itemManagerInstance = await ItemManager.deployed();

        const result = await itemManagerInstance.createItem("test", itemPrice, { from: accounts[0] });
        await itemManagerInstance.triggerPaymentDirectlyOnStorage(result.logs[0].args._itemAddress, { value: web3.utils.toWei(new BN(itemPrice), "wei") });
        const resultDelivery = await itemManagerInstance.triggerDelivery(result.logs[0].args._itemAddress);

        const item = await itemManagerInstance.getItemByAddress(result.logs[0].args._itemAddress);
        // checking event -> library?
        assert.equal(resultDelivery.logs[0].args._step, 2, "Item in wrong status");
        // checking object state
        assert.equal(item._step, 2, "Item in wrong status")
    })
});