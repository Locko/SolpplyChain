
const SolpplyChainToken = artifacts.require("SolpplyChainToken");

var chai = require("./setupchai.js");
const BN = web3.utils.BN;
const expect = chai.expect;

require("dotenv").config({ path: "../.env" });

contract("SolpplyChainToken Test", async (accounts) => {

    const [deployerAccount, recipient, anotherAccount] = accounts;

    // We dont use the .deployed() contracts, because they are in a state in which the crowdsale was triggered
    // and the inital supply was drained from the deployer account
    beforeEach(async () => {
        this.solpplyChainTokenIsolated = await SolpplyChainToken.new(process.env.INITIAL_TOKENS);
    });

    it("All Tokens should be in my account intially", async () => {
        let instance = this.solpplyChainTokenIsolated
        let totalSupply = await instance.totalSupply();
        return await expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(totalSupply);
    });

    it("is possible to send tokens between accounts", async () => {
        const sendTokens = 1;
        let instance = this.solpplyChainTokenIsolated
        let totalSupply = await instance.totalSupply();
        expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(totalSupply);
        // TODO lspiller wieso failed der test ohne das await hier? sollte .eventually nicht schon auf die erfüllung des promise warten?
        await expect(instance.transfer(recipient, sendTokens)).to.eventually.be.fulfilled;
        expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(totalSupply.sub(new BN(sendTokens)));
        return await expect(instance.balanceOf(recipient)).to.eventually.be.a.bignumber.equal(new BN(sendTokens));
    });

    it("is not possible to send more token than available in total", async () => {
        let instance = this.solpplyChainTokenIsolated;
        let balanceOfDeployer = await instance.balanceOf(deployerAccount);

        expect(instance.transfer(recipient, new BN(balanceOfDeployer + 1))).to.eventually.be.rejected;
        return await expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(balanceOfDeployer);
    });



});