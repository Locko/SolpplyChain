const TokenSale = artifacts.require("SolpplyChainTokenSale");
const Token = artifacts.require("SolpplyChainToken");
const KycContract = artifacts.require("KycContract");

const { async } = require("rxjs");
const { web } = require("webpack");
var chai = require("./setupchai.js");
const BN = web3.utils.BN;
const expect = chai.expect;

require("dotenv").config({ path: "../.env" });

contract("SolpplyChainTokenSale Test", async (accounts) => {

    const [deployerAccount, recipient, anotherAccount] = await accounts;


    it("should not have any tokens in my deployerAccount", async () => {
        let instance = await Token.deployed();
        return await expect(instance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(new BN(0));
    });

    it("all token should be in the TokenSale Contract by default", async () => {
        let instance = await Token.deployed();
        // The token has a reference to the tokensale address via the constructor call inside the deploy_contracts.js
        let balanceOfTokenSaleContract = await instance.balanceOf(TokenSale.address);
        let totalSupply = await instance.totalSupply();
        return await expect(balanceOfTokenSaleContract).to.be.a.bignumber.equal(totalSupply);
    });

    it("should be possible to buy token", async () => {
        let tokenInstance = await Token.deployed();
        let tokenSaleInstance = await TokenSale.deployed();
        let kycInstance = await KycContract.deployed();
        let balanceBefore = await tokenInstance.balanceOf(deployerAccount);
        // deployerAccount whitelists itself
        await kycInstance.setKycCompleted(deployerAccount, { from: deployerAccount });
        await expect(tokenSaleInstance.sendTransaction({ from: deployerAccount, value: web3.utils.toWei("1", "wei") })).to.be.fulfilled;
        return await expect(tokenInstance.balanceOf(deployerAccount)).to.eventually.be.a.bignumber.equal(balanceBefore.add(new BN(1)));
    });

});
