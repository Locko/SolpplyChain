import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'StepPipe' })
export class StepPipe implements PipeTransform {


    transform(value: String): string {
        switch (value) {
            case '0': return StepPipe.Step.CREATED;
            case '1': return StepPipe.Step.PAID;
            case '2': return StepPipe.Step.DELIVERED;
        }
    }
}

export namespace StepPipe {
    export enum Step {
        CREATED = "Created",
        PAID = "Paid",
        DELIVERED = "In Delivery"
    }
}