import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import ItemManager from '../../../build/contracts/ItemManager.json';
import Item from '../../../build/contracts/Item.json';
import { ItemDTO } from './DTO/ItemDTO';
declare let window: any;
declare let require: any;
const Web3 = require('web3');

@Component({
  selector: 'app-item-manager',
  templateUrl: './item-manager.component.html',
  styleUrls: ['./item-manager.component.css']
})
export class ItemManagerComponent implements OnInit {
  accounts: string[];
  itemManager: any;
  item: any;
  private web3: any;
  networkId: any;
  status = 'idle';
  currentItemAddress: any;
  userItems: ItemDTO[] = [];

  model = {
    cost: 0,
    itemName: 'Example Item',
  };


  constructor(private matSnackBar: MatSnackBar) {
    window.addEventListener('load', () => {
      console.log('window event')
      this.initContracts();
    });
  }

  ngOnInit(): void {
    this.initContracts();
  }

  async initContracts() {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.ethereum !== 'undefined') {
      // Use Mist/MetaMask's provider
      await window.ethereum.enable().then(() => {
        this.web3 = new Web3(window.ethereum);
      });
    } else {
      console.log('No web3? You should consider trying MetaMask!');
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:9545'));
    }

    this.accounts = await this.web3.eth.getAccounts();
    const networkId = await this.web3.eth.net.getId();

    this.itemManager = new this.web3.eth.Contract(
      ItemManager.abi,
      ItemManager.networks[networkId] && ItemManager.networks[networkId].address);

    this.item = new this.web3.eth.Contract(
      Item.abi, Item.networks[networkId] && Item.networks[networkId].address);

    // this.listenToPaymentEvent();
    this.listItemsOfUser();
  }

  /** 
  async listenToPaymentEvent() {
    let self = this;
    this.itemManager.events.SupplyChainStep().on('data', async function (e) {
      console.log('SupplyChainStep Event: ', JSON.stringify(e));
      let itemObj = await self.itemManager.methods.items(e.returnValues._customerAddress).call();
      console.log('Event für Item: ', JSON.stringify(itemObj));
      if (itemObj._step === 1) {
        alert("Item " + itemObj._identifier + " was paid, deliver it now!");
        console.log("Item " + itemObj._identifier + " was paid, deliver it now!");
      }
    });
  }
  */

  async handleSubmit() {
    let self = this;
    if (!this.itemManager) {
      this.setStatus('ItemManager is not loaded, unable to send transaction');
      console.log('ItemManager is not loaded, unable to send transaction')
      return;
    }
    console.log("Submitting to the blockchain!");
    this.setStatus('Initiating transaction... (please wait)');
    const cost = this.model.cost;
    const itemName = this.model.itemName;

    try {
      let transaction = await this.itemManager.methods.createItem(itemName, cost).send({ from: this.accounts[0] });
      this.currentItemAddress = transaction.events.SupplyChainStep.returnValues._address;
    } catch (exception) {
      console.log(exception);
    }
    this.listItemsOfUser();
  }

  async listItemsOfUser() {
    let self = this;
    if (!this.itemManager) {
      this.setStatus('ItemManager is not loaded, unable to send transaction');
      console.log('ItemManager is not loaded, unable to send transaction')
      return;
    }
    try {
      var userItems: ItemDTO[] = await this.itemManager.methods.getItemsForCustomerAddress(this.accounts[0]).call();
      this.userItems = userItems;
    }
    catch (ex) {
      console.log(ex);
    }
  }

  async payItem(itemAddress: string, itemPrice: number) {

    let self = this;
    if (!this.itemManager) {
      this.setStatus('ItemManager is not loaded, unable to send transaction');
      console.log('ItemManager is not loaded, unable to send transaction')
      return;
    }

    this.setStatus('Initiating transaction... (please wait)');
    console.log("item address: " + itemAddress);
    console.log("item price: " + itemPrice);

    try {
      let transaction = await this.itemManager.methods.triggerPaymentDirectlyOnStorage(itemAddress).send({ from: this.accounts[0], value: itemPrice });
      // transaction.events.SupplyChainStep.returnValues._itemAddress
      this.listItemsOfUser();
    }
    catch (ex) {
      console.log(ex);
    }
  }

  async triggerDelivery(itemAddress: string) {
    let transaction = await this.itemManager.methods.triggerDelivery(itemAddress).send({ from: this.accounts[0] });
    console.log("Delivery receipt: " + JSON.stringify(transaction))
    this.listItemsOfUser();
  }

  setStatus(status) {
    this.status = status;
    this.matSnackBar.open(this.status, null, { duration: 3000 });
  }

  setCostInWei(e) {
    this.model.cost = e.target.value;
  }

  setItemName(e) {
    this.model.itemName = e.target.value;
  }








}
