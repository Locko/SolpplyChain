
export class ItemDTO {
    public _item: string;
    public _identifier: string;
    public _itemPrice: number;
    public _step: number;
    public _customerAddress: string;

    constructor(_item: string, _identifier: string, _itemPrice: number, _step: number, _customerAddress: string) {
        this._item = _item;
        this._identifier = _identifier;
        this._itemPrice = _itemPrice;
        this._step = _step;
        this._customerAddress = _customerAddress;
    }


}