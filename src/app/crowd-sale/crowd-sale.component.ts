import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import SolpplyChainToken from '../../../build/contracts/SolpplyChainToken.json';
import SolpplyChainTokenSale from '../../../build/contracts/SolpplyChainTokenSale.json';
import KycContract from '../../../build/contracts/KycContract.json';
declare let window: any;
declare let require: any;
const Web3 = require('web3');

@Component({
  selector: 'app-crowd-sale',
  templateUrl: './crowd-sale.component.html',
  styleUrls: ['./crowd-sale.component.css']
})
export class CrowdSaleComponent implements OnInit {

  private web3: any;
  accounts: string[];
  solpplyChainToken: any;
  solpplyChainTokenSale: any;
  kycContract: any;
  state: boolean = false;

  model = {
    kycAddress: '0x123',
    tokenSaleAddress: '',
    userTokens: 0,
    tokenAmount: 0
  };

  constructor(private matSnackBar: MatSnackBar, private changeDetector: ChangeDetectorRef) {
    window.addEventListener('load', async () => {
      if(!this.initInProgress) {
        await this.initContracts();
      }
    });
  }

  ngOnInit() {
    if (!this.initInProgress) {
      this.initContracts();
    }
  }

  initInProgress = false;


  // TODO lspiller extract the contract initialization logic into a base class
  async initContracts() {
    if (this.initInProgress) return;
    this.initInProgress = true;
  
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.ethereum !== 'undefined') {
      // Use Mist/MetaMask's provider
      try {
        await window.ethereum.request({ method: 'eth_requestAccounts' }); // Updated line
        //this.web3 = new Web3(window.ethereum);
        this.web3 = new Web3(window.web3.currentProvider);
      } catch (error) {
        console.error("User denied account access:", error);
      } finally {
        this.initInProgress = false;
      }
    }
    else if (window.web3) {
        this.web3 = new Web3(window.web3.currentProvider);
    } else {
      console.log('No web3? You should consider trying MetaMask!');
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
    }

    this.accounts = await this.web3.eth.getAccounts();
    const networkId = await this.web3.eth.net.getId();

    this.solpplyChainToken = new this.web3.eth.Contract(
      SolpplyChainToken.abi,
      SolpplyChainToken.networks[networkId] && SolpplyChainToken.networks[networkId].address,
    );

    this.solpplyChainTokenSale = new this.web3.eth.Contract(
      SolpplyChainTokenSale.abi,
      SolpplyChainTokenSale.networks[networkId] && SolpplyChainTokenSale.networks[networkId].address,
    );
    this.kycContract = new this.web3.eth.Contract(
      KycContract.abi,
      KycContract.networks[networkId] && KycContract.networks[networkId].address,
    );

    this.listenToTokenTransfer();
    this.state = true;
    this.model.tokenSaleAddress = SolpplyChainTokenSale.networks[networkId].address;
    this.updateUserTokens();
  }

  async handleSubmit() {
    if (!this.kycContract) {
      console.log('ItemManager is not loaded, unable to send transaction')
      return;
    }

    const kycAddress = this.model.kycAddress;

    try {
      let transaction = await this.kycContract.methods.setKycCompleted(kycAddress).send({ from: this.accounts[0] });
      alert("KYC for " + kycAddress + " is completed");
    } catch (exception) {
      console.log(exception);
    }
  }

  async updateUserTokens() {
    // calls are free reading operations
    let userTokens = await this.solpplyChainToken.methods.balanceOf(this.accounts[0]).call();
    this.model.userTokens = userTokens;
    this.changeDetector.detectChanges();
  }

  async handleBuyToken() {
    await this.solpplyChainTokenSale.methods.buyTokens(this.accounts[0]).send({ from: this.accounts[0], value: this.web3.utils.toWei(this.model.tokenAmount, "wei") });
  }

  getUserTokens(): number {
    return this.model.userTokens;
  }

  listenToTokenTransfer() {
    this.solpplyChainToken.events.Transfer({ to: this.accounts[0] }).on('data', () => this.updateUserTokens());
  }

  setKycAddress(e) {
    this.model.kycAddress = e.target.value;
  }

  setTokenAmount(e) {
    this.model.tokenAmount = e.target.value;
  }

}
