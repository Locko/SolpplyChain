import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrowdSaleComponent } from './crowd-sale.component';

describe('CrowdSaleComponent', () => {
  let component: CrowdSaleComponent;
  let fixture: ComponentFixture<CrowdSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrowdSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrowdSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
