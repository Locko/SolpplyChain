import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ItemManagerComponent } from './item-manager/item-manager.component';
import { CrowdSaleComponent } from './crowd-sale/crowd-sale.component';


const routes: Routes = [
  { path: 'items', component: ItemManagerComponent },
  { path: 'crowd-sale', component: CrowdSaleComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
