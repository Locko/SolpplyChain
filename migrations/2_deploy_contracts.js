var ItemManager = artifacts.require("./ItemManager.sol");
var SolpplyChainToken = artifacts.require("./SolpplyChainToken.sol");
var SolpplyChainTokenSale = artifacts.require("./SolpplyChainTokenSale");
var KycContract = artifacts.require("KycContract");
require("dotenv").config({ path: "../.env" });

module.exports = async function (deployer) {
  deployer.deploy(ItemManager);
  let addr = await web3.eth.getAccounts();
  // Creater of contract mints the initial supply (1000000)
  await deployer.deploy(SolpplyChainToken, process.env.INITIAL_TOKENS);
  let deployerAccount = addr[0];
  await deployer.deploy(KycContract);
  // at a 1:1 rate the deployer account will receive the Tokens after payment
  await deployer.deploy(SolpplyChainTokenSale, 1, deployerAccount, SolpplyChainToken.address, KycContract.address);
  let tokenInstance = await SolpplyChainToken.deployed();
  // the initially minted supply is sent to the crowdsale address
  tokenInstance.transfer(SolpplyChainTokenSale.address, process.env.INITIAL_TOKENS);

};


