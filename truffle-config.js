require("dotenv").config({
  path: "./.env"
});
const HDwalletProvider = require("@truffle/hdwallet-provider");
const AccountIndex = 0;
const mnemonic = "month industry throw grass unaware enlist black coin potato alcohol quarter excess";
module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*"
    },
    goerli_infura: {
      provider: function () {
        return new HDwalletProvider(mnemonic, "https://goerli.infura.io/v3/107a4257d59b4cb298480f2f69daab30", AccountIndex);
      },
      network_id: "5"
    },
    ropsten_infura: {
      provider: function () {
        return new HDwalletProvider(mnemonic, "https://ropsten.infura.io/v3/107a4257d59b4cb298480f2f69daab30", AccountIndex);
      },
      network_id: "3"
    },
    loc_development_development: {
      network_id: "*",
      port: 8545,
      host: "127.0.0.1"
    }
  },
  compilers: {
    solc: {
      version: "0.8.13"
    }
  }
};
0x8955984F49dC3382E860C2FB973187357a12a635;
